.. index:: sandwich

Pittsburgh Style Sandwich
===============================================================================

Primanti Brothers is a chain of sandwich shops in western Pennsylvania, in the United States. Founded in Pittsburgh, it is known for its signature sandwiches of grilled meat, melted cheese, an oil & vinegar-based coleslaw, tomato slices, and French fries between two thick slices of Italian bread.

This recipe is a variation on the classic Primanti Brothers style sandwich, using an entire loaf of Italian bread, which is from an even more local variation in Erie, PA.


Ingredients
-------------------------------------------------------------------------------
- 1 loaf Italian bread

- 1/2 kg (1 lb) french fries ( regular cut, fresh cut are always great! )

- Deli meats of your choice ( Turkey, Pastrami, Corned Beef, Roast Beef, Capicola, Salami and Keilbasa are great choices )

- Deli cheese ( provolone is an excellent choice )

- Fried eggs ( Optional )

- Hot sauce ( Optional, Frank's Red Hot is traditional )

- 125 ml (1/2 cup) apple cider vinegar

- 60 ml (1/4 cup) vegetable oil ( canola will also work )

- 50 ml (4 tbsp) celery seed

- 15 ml (1 tbsp) onion powder

- 1 head of green cabbage, sliced thin

- salt and pepper to taste

Preparation
-------------------------------------------------------------------------------

1.  Combine the vinegar, vegetable oil, sugar, salt, onion powder, and pepper in a large mixing bowl. Whisk until well mixed.

2.  Add the cabbage, toss until evenly coated.

3.  You have now made Pittsburgh style slaw! Refrigerate until needed.

4. Prepare your french fries in your preferred style

5. Slice your loaf of Italian bread in half horizontally

6. Add meat, cheese, slaw, and french fries on top. If the slaw and fries are starting to fall off, you'll know you've added the correct amount.

7. Add hot sauce and/or fried eggs on top

8. Top with the second half of the French bread, press firmly over the top of bread to give the sandwich a fighting chance of staying together

9. Enjoy! A fork is a recommended accessory, as there is a 100% chance of coleslaw and fries ending up on your plate!

Kyle M Hall

Kyle is a developer in the Koha community, and has acted as Release Maintainer and Release Manager, as well as a fixture on the QA team.
