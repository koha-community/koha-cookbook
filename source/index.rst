.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Koha Cookbook!
=============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Introduction
-------------------

Not much to see here yet.. we're working on it.

Mains
-------------------
.. toctree::
   :maxdepth: 1

   henfricassee
   pghsandwich
   ribsandcoleslaw


Drinks
-------------------
.. toctree::
   :maxdepth: 1

   dandelionshrubmanhattan

Desserts
-------------------
.. toctree::
   :maxdepth: 1

   poudingchomeur
   pricklypearcactusicecream

Indices and tables
-------------------

* :ref:`genindex`
* :ref:`search`
