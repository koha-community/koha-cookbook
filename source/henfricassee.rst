.. index:: comfort food, stew, entertaining

Hen Fricassee
===============================================================================

Everyone loves chicken. That means there must be a lot of hens somewhere. But
who eats hens? In Norway they are used to make cement (true story). Making hen
fricassee is probably better use of resources. And when the result tastes as
good as this, "doing the right thing" has never been easier. The secret here is
to boil the hen for a really long time, both to make the meat tender and to make
the broth that is the basis for the rest of the dish. The ingredients can be
doubled, to make nice leftovers. You can also vary this after your own heart,
with other vegetables or by adding different herbs.


.. image:: images/dishes/henfricassee.jpg


Ingredients
-------------------------------------------------------------------------------

-  1 hen

-  1 bay leaf

-  5 pepper corns

-  1 onion, cut in 4

-  Water

-  50 grams butter

-  50 grams white flour

-  1 carrot

-  1/2 turnip

-  1/2 leek

-  4 potatoes 

-  100 ml cream

-  Parsley

Preparation
-------------------------------------------------------------------------------

1.  Put the hen, salt, pepper, and onion in a deep pan. Cover with water. Boil
    for about 2 hours, or until the leg easily separates from the body if you
    try to lift it out.

2.  When the hen is really tender, take it out and pass the broth through a sieve.

3.  Remove the meat from the bones, and cut large pieces into smaller pieces and
    set aside.

4.  Peel all the vegetables and cut them into cubes, about 1 cm on either side.

5.  Fry the vegetables quickly in the butter, stir in the flour and add about
    1000 ml of the broth. Let it simmer until the vegetables are cooked.

6.  Add the cream and the hen meat. Add some chopped parsley or other herbs.


-------------------------------------------------------------------------------

Magnus Enger, Bodø, Norway

Librarian with a master in Science of documentation, foodie, father, puffin dog
lover, living in Bodø, Norway - above the Arctic circle! Started Libriotech
as a Koha support company in 2009, and has been working on Koha ever since.
