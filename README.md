![Build Status](https://gitlab.com/koha-community/koha-cookbook/badges/master/pipeline.svg)

---

# koha-cookbook

A collection of recipes inspired by Koha users and contributors around the world.

## Submitting your recipe

### Requirements

Note that the Koha cookbook is a public domain (CC0) project. Therefore, recipes must be original or very modified. Do not submit a recipe that is not your own, taken from a website, a book or a magazine.

Ingredients must be given in metric measurements

  - grams (g) for dry ingredients 

  - milliliters (ml) for wet ingredients 

  - celcius (°C) for temperatures 

All types of recipes (drink, cocktail, beer, bread, mains, desserts, snacks, breakfast, etc.) are welcome!

### Write your recipe

Use the recipe template available at https://gitlab.com/koha-community/koha-cookbook/-/blob/master/recipetemplate.rst to write your recipe.

### Submit your recipe

You can use gitlab to submit your recipe.

   1. GitLab account: First of all, you have to create an account on GitLab. You can also sign in with an existing Google, Twitter, GitHub or BitBucket account.
      The first time you connect to Gitlab, go to https://gitlab.com/koha-community/koha-cookbook and click "Fork". This will make a copy of the manual in your account. You will always work in your own copy and then make "merge requests". The Cookbook team will then check your work to make sure there are no errors and then merge your work into the real cookbook. This helps avoid errors. 
    
   2. Download the template: in the project files, there should be a file called recipetemplate.rst, click on the file name, then click on Download and save on your computer

      ![Screenshot showing the location of the download button](https://wiki.koha-community.org/w/images/Download_recipe_template.png)

   3. Edit the file: Open the file in your favorite text editor and write in your recipe; make sure to save under another name
    
   4. Upload the file on gitlab:
      
      - In your project, navigate to the "source" directory 

       ![Screenshot showing the source directory](https://wiki.koha-community.org/w/images/Cookbook_source.png)

      - Click on the + button and choose "Upload file" 

       ![Screenshot showing the Upload file option in the + button](https://wiki.koha-community.org/w/images/Cookbook_upload_button.png)

      - Choose the file, enter a commit message (something like "added [name of recipe]") and click "upload file" 

       ![Screenshot showing the upload file page](https://wiki.koha-community.org/w/images/Cookbook_upload_file.png)

   5. Create a merge request:
       
      - In the left side menu, click on "Merge requests" 

       ![Screenshot showing the Merge requests link in the left menu](https://wiki.koha-community.org/w/images/Cookbook_mergerequest_menu.png)

      - Click "New merge request"

      - On the left, you should see yourname/koha-cookbook; click "Select source branch" and choose master 

       ![Screenshot showing the merge request page](https://wiki.koha-community.org/w/images/Cookbook_create_mergerequest.png)

      - On the right, you should see koha-community/koha-cookbook and the master branch is already selected

      - Click on "Compare branches and continue"

      - If needed, edit your commit message, and add a description (optional)

      - Click "Submit merge request" 

If you are not comfortable with this process, you can submit your recipe by email.

   1. Download the template: go to https://gitlab.com/koha-community/koha-cookbook/-/blob/master/recipetemplate.rst , click Download and save the file on your computer

      ![Screenshot showing the location of the download button](https://wiki.koha-community.org/w/images/Download_recipe_template.png)

   2. Edit the file: Open the file in your favorite text editor and write in your recipe; make sure to save under another name
    
   3. Send your file to koha_cookbook [at] protonmail.com 

## More information 

Find more information about the project on the Koha Community wiki https://wiki.koha-community.org/wiki/Koha_cookbook

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

## Building locally

### Prerequisites

> sudo apt-get install make python3-sphinx python3-sphinxcontrib.spelling python3-pip

> pip3 install -U sphinx-book-theme

### Workflow

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Generate the documentation: `make html`

The generated HTML will be located in the location specified by `conf.py`,
in this case `_build/html`.

