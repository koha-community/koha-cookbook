.. Enter tags for your recipe. In this example, the recipe has two tags, 
   dessert and cake.
.. index:: dessert, cake

Name of recipe
===============================================================================

Short description of the dish


.. image:: images/dishes/picture-of-the-dish.jpg


Ingredients
-------------------------------------------------------------------------------

-  ingredient 1

-  ingredient 2

-  ingredient 3

-  For international purposes, ingredients will need to be indicated in
   metric measurements (grams for dry ingredients, milliliters for wet
   ingredients, celsius for temperatures). If you really want to, you can
   indicate the imperial measurement next to the metric one.



Preparation
-------------------------------------------------------------------------------

1.  Step 1

2.  Step 2 Note that if the description of the step goes over one line, the
    following lines should be indented.

3.  Step 3

4.  Step 4

5.  Step 5


-------------------------------------------------------------------------------

Name of contributor, Country of the contributor

Short description of the contributor and their connection to Koha
